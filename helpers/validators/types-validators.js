const { body } = require('express-validator');
const { errorsMessages } = require('../../consts/response-messages');
const { badData } = errorsMessages;

function isString(field) {
    const message = {
        isString: `${field} ${badData.validationErrors.types.string }`
    };
    return body(field, message).isString();
}

module.exports = { isString }