const { body } = require('express-validator');

function isEmpty(field) {
    return body(field).isEmpty();
}

module.exports = { isEmpty };