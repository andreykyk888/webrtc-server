const { body } = require('express-validator');
const { errorsMessages } = require('../../consts/response-messages');

function checkMinLength(field, length) {
    const message = {
        minLength: `${field} ${errorsMessages.badData.validationErrors.min} ${length} symbols`
    };
    return body(field, message).isLength({ min: length });
}

function checkMaxLength(field, length) {
    const message = {
        maxLength: `${field} ${errorsMessages.badData.validationErrors.max} ${length} symbols`
    };
    return body(field, message).isLength({ max: length });
}

module.exports = { checkMinLength, checkMaxLength };