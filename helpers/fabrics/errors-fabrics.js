const { ErrorResponse } = require('../../classes');

class ErrorsFabrics {
    static createValidationErrors(validationErrors) {
        const errors = {};
        validationErrors.forEach(error => {
            if(errors[error.param]) {
                errors[error.param] = {...errors[error.param], ...error.msg};
            } else {
                errors[error.param] = {...error.msg};
            }
        });
        return new ErrorResponse(errors);
    }

     static createError(error, field, errorKey) {
        const responseError = {
            [field]: {
                [errorKey]: error
            }
        };
       return new ErrorResponse(responseError);
     }
}

module.exports = ErrorsFabrics;