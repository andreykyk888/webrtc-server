const ErrorsFabrics = require('./errors-fabrics');
const ResultFabrics = require('./result-fabrics');

module.exports = { ErrorsFabrics, ResultFabrics }