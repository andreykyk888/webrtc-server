const { ErrorsFabrics } = require('../fabrics');
const { ResultFabrics } = require('../fabrics');

class ResponseBuilder {
    static _buildResponse(res, status, message) {
        if(message) {
            return res.status(status).json(message);
        } else {
            return res.sendStatus(status);
        }
    }

    static buildResponseValidationError(res, status, errors) {
        const transformedError = ErrorsFabrics.createValidationErrors(errors);
        return ResponseBuilder._buildResponse(res, status, transformedError);
    }

    static buildResponseError(res, status, error, field, errorKey) {
        const transformedError = ErrorsFabrics.createError(error, field, errorKey);
        return ResponseBuilder._buildResponse(res, status, transformedError);
    }

    static buildResponseDefaultError(res, status, error) {
        return ResponseBuilder._buildResponse(res, status, error);
    }

    static buildResponseResult(res, status, result) {
        const message = ResultFabrics.createDefaultResult(result);
        return ResponseBuilder._buildResponse(res, status, message);
    }
}

module.exports = ResponseBuilder;