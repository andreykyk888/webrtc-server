const { Schema, model } = require('mongoose');

const userModel = new Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
});

module.exports = model('user', userModel);