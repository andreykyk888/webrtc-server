const { Router } = require("express");
const { WS_EVENTS } = require("../../../consts/ws-events");
const { WS_REQUEST_TYPES } = require("../../../consts/ws-request-types")
const wSServer = global.wSServer;
const router = Router();
const aWss = wSServer.getWss();

router.ws('', (ws, req) => {
    listenData(ws);
});

const listenData = (ws) => {
    ws.on(WS_EVENTS.message, (data) => {
        const parsedData = JSON.parse(data);
        switch (parsedData.reqType) {
            case WS_REQUEST_TYPES.initData:
                saveSession(ws, parsedData);
                sendGreeting(ws, parsedData);
                break;
            default:
                sendRTC(parsedData);
                break;
        }
    });
};

const saveSession = (ws, user) => {
    ws.id = user.username;
}

const sendGreeting = (ws, user) => {
    const jsonUser = JSON.stringify(user)
    ws.send(jsonUser);
};

const sendRTC = (data) => {
    const sender = data.sender;
    for (let client of aWss.clients) {
        if (sender.username === data.receiverUsername) return;
        if (client.id === data.receiverUsername) {
            console.log(1);
            const jsonData = JSON.stringify(data);
            client.send(jsonData);
        }
    }
};

const getWsUser = (value) => {
    return Array.from(aWss.clients).find(client => client.id === value);
}


module.exports = router;
