const { Router } = require('express');
const { ROUTES } = require('../../../../consts/routes');
const UserService = require('../../services/user.service');
const { ResponseBuilder } = require('../../../../helpers/builders');
const { validationResult } = require('express-validator');
const { errorsStatuses, successesStatuses } = require('../../../../consts/response-statuses');
const USER_ERROR_MESSAGES = require('../../consts/user-error-messages');
const UserValidatorFacades = require('../../validators/user-validator-facades')
const { errorsMessages } = require('../../../../consts/response-messages');
const router = Router();

router.post(
    `/${ROUTES.users.home}/${ROUTES.users.register}`,
    UserValidatorFacades.validateRegisterUser(),
    async (
        req, res
    ) => {
        try {
            const { errors } = validationResult(req);
            if (errors.length) {
                return ResponseBuilder.buildResponseValidationError(res, errorsStatuses.badData, errors);
            }
            const user = req.body;
            const newUser = await UserService.saveUser(user);
            return ResponseBuilder.buildResponseResult(res, successesStatuses.successPost, newUser);
        } catch (e) {
            return ResponseBuilder.buildResponseDefaultError(res, errorsStatuses.serverError, errorsMessages.serverError);
        }
    });

router.post(
    `/${ROUTES.users.home}/${ROUTES.users.login}`,
    UserValidatorFacades.validateLoginUser(),
    async (req, res) => {
        try {
            const { errors } = validationResult(req);
            if (errors.length) {
                return ResponseBuilder.buildResponseValidationError(res, errorsStatuses.badData, errors);
            }
            const { username } = req.body;
            const user = await UserService.findUser({ username });
            if (!user) {
                const field = 'username';
                const errorKey = 'doesntExist';
                const message = `${field} ${username} ${USER_ERROR_MESSAGES.doesntExistUser}`;
                return ResponseBuilder.buildResponseError(res, errorsStatuses.notFound, message, field, errorKey);
            } else {
                return ResponseBuilder.buildResponseResult(res, successesStatuses.successPost, user);
            }
        } catch (e) {
            return ResponseBuilder.buildResponseDefaultError(res, errorsStatuses.serverError, errorsMessages.serverError);
        }
    });

module.exports = router;
