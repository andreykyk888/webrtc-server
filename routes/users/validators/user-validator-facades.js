const { USER_DATA_LENGTHS } = require('../../../consts/data-lengths');
const { min, max } = USER_DATA_LENGTHS;
const { LENGTH_VALIDATORS } = require('../../../helpers/validators');
const { TYPES_VALIDATORS } = require('../../../helpers/validators');
const { isExistingUsername } = require('./is-existing-validator')
const { checkMinLength, checkMaxLength } = LENGTH_VALIDATORS;
const { isString } = TYPES_VALIDATORS

class UserValidatorFacades {
    static _fields = {
        username: 'username',
        firstName: 'firstName',
        lastName: 'lastName'
    };

    static validateRegisterUser() {
        const errors = [];
        const existingError = isExistingUsername();
        errors.push(existingError);
        for (let value of Object.values(UserValidatorFacades._fields)) {
            errors.push(checkMinLength(value, min[value]));
            errors.push(checkMaxLength(value, max[value]));
            errors.push(isString(value));
        }
        return errors.filter(v => !!v);
    };

    static validateLoginUser() {
        const field = UserValidatorFacades._fields.username;
        const errors = [];
        errors.push(checkMinLength(field, min[field]));
        errors.push(checkMaxLength(field, max[field]));
        errors.push(isString(field));
        return errors.filter(v => !!v);
    };
}

module.exports = UserValidatorFacades;