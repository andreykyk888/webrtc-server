const { body } = require('express-validator');
const USER_ERROR_MESSAGES = require('../consts/user-error-messages');
const UserService = require('../services/user.service');

function isExistingUsername() {
    return body('username').custom(async (value) => {
        return UserService.findUser({ username: value }).then(user => {
            if (user) {
                const message = `${value} ${USER_ERROR_MESSAGES.existingUsername}`
                return Promise.reject({ isExisting: message });
            }
            return false;
        })
    });
}

module.exports = { isExistingUsername };