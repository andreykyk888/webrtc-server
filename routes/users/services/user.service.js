const UserModel = require('../../../models/user.model');

class UserService {
   static async findUser(param) {
      return UserModel.findOne(param);
   }

   static async saveUser(user) {
      const newUser = new UserModel(user);
      return newUser.save();
   }
}

module.exports = UserService;