const USER_ERROR_MESSAGES = {
    existingUsername: 'is existing username',
    doesntExistUser: 'doesn`t exist'
}

module.exports = USER_ERROR_MESSAGES;