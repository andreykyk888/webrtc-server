const usersRestRoutes = require('./users/controllers/rest/users-rest')
const callConnectionRoutes = require('./ws/controllers/call-connection-ws')

module.exports = { usersRestRoutes, callConnectionRoutes }