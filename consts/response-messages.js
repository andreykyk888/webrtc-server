const VALIDATION_ERROR_MESSAGES = {
    min: 'should contain min',
    max: 'should contain max',
    types: {
        string: 'should be string'
    }
}

const RESPONSE_MESSAGES = {
    errorsMessages: {
        serverError: 'something went wrong',
        notFound: 'not found',
        badData: {
            existing: 'already for existing',
            validationErrors: VALIDATION_ERROR_MESSAGES,
            default: 'bad data',
        }
    },
    successesMessages: {
        statusOk: 'OK'
    }
}

module.exports = RESPONSE_MESSAGES;