const USER_DATA_LENGTHS = {
    min: {
        username: 5,
        firstName: 2,
        lastName: 2,
    },
    max: {
        username: 40,
        firstName: 20,
        lastName: 20,
    }
}

module.exports = { USER_DATA_LENGTHS };