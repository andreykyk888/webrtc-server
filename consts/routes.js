const ROUTES = {
    home: '/api',
    users: {
        home: 'users',
        login: `login`,
        register: `register`,
        username: ':username',
    },
}

const WS_ROUTES = {
    home: '/api',
};

module.exports = { ROUTES, WS_ROUTES };