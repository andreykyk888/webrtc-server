const STATUSES = {
    errorsStatuses: {
        badData: 400,
        notAuth: 401,
        notFound: 404,
        serverError: 500,
    },
    successesStatuses: {
        successGet: 200,
        successPost: 201
    }
}

module.exports = STATUSES;