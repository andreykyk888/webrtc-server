require('dotenv').config();
const express = require('express');
const app = express();
const wSServer = require('express-ws')(app);
const mongoose = require('mongoose');
const cors = require('cors');
global.wSServer = wSServer;

const { ROUTES, WS_ROUTES } = require('./consts/routes');
const { usersRestRoutes } = require('./routes');
const { callConnectionRoutes } = require('./routes')

app.use(cors());
app.use(express.json());

app.use(`${ROUTES.home}`, usersRestRoutes);
app.use(`${WS_ROUTES.home}`, callConnectionRoutes);

const start = async () => {
    try {
        await mongoose.connect(process.env.DB_PATH, {}, (err) => {
            if(err) {
                throw err;
            }
        });
    } catch (e) {
        console.log(e);
    }
}
start();

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`server started on PORT ${PORT}`));