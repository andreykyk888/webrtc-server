class ErrorResponse {
    constructor(errors) {
        this.errors = errors;
    }
}

module.exports = ErrorResponse;