const ResultResponse = require('./result-response');
const ErrorResponse = require('./error-response');

module.exports = {ResultResponse, ErrorResponse}